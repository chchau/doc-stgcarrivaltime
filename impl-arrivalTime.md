Studies of the time of arrival of electron clusters were carried out using 
samples produced with Garfield. The time of arrival was characterized in 
terms of the charged particle's distance of closest approach.
Information about the simulation studies can be found at: 
https-AAA

This wiki page elaborates on the implementation of the sTGC time of arrival in
the ATLAS software. It is already integrated in the sTGC_Digitization package of 
the Athena master branch. Therefore, the time of arrival is currently the main 
component of the sTGC time response in the sTGC digitization.

**On this page**

[[_TOC_]]

## Parametrization

A 2-dimensional histogram of the electron time of arrival vs the distance of 
closest approach is shown below:
<img src="impl_arrivalTime2D.png" width="696" height="471">

Implementing this 2D histogram in Athena is possible; however, it is not easy
to maintain in the future. So a simpler model was established and integrated 
in the Athena master branch. Obviously, the model is less accurate. 

The parametrization is achieved by binning the 2D histogram above into eleven 
bins that are referred to as distance intervals. The 11 distance intervals are 
labeled from one to eleven as following:

1. [0.00, 0.05[ mm
2. [0.05, 0.15[ mm
3. [0.15, 0.25[ mm
4. [0.25, 0.35[ mm
5. [0.35, 0.45[ mm
6. [0.45, 0.55[ mm
7. [0.55, 0.65[ mm
8. [0.65, 0.75[ mm
9. [0.75, 0.80[ mm
10. [0.80, 0.85[ mm
11. [0.85, 0.90[ mm

The distance intervals have a width of 100 microns, except for the first and the 
last three intervals. Interval 1 is 0.50mm-wide because of the proximity to the
wire. As for the Intervals 9, 10 and 11, the most probable time of arrival 
above 0.75~mm rises quickly. So narrow interval leads to a more accurate description. 

It was found that the time of arrival in each interval follows a gamma 
distribution. Therefore, a gamma probability density function (pdf) can be 
used to describe the distribution of time of arrival: 

```math
p(t) = \frac{1}{\Gamma(k) \theta^k} (t - t_0)^{k-1} e^{-(t - t_0)/\theta}
```

The parameter $`k`$ acts upon the rising part of the gamma pdf, while $`\theta`$
determines the falling part. The parameter $`t_0`$ shifts the gamma pdf along 
the $`t`$-axis. Therefore, the most probable time of arrival ($`mpt`$) for 
Intervals 2-11 is given by:

```math
mpt = t_0 + (k - 1) \theta
```

The $`mpt`$ formula above is not valid for Interval 1, since $`k-1`$ is negative.
Also $`mpt`$ for Interval 1 is set to zero nanosecond.

To simplify the implementation, the parameters $`k`$ and $`\theta`$ of an interval 
are assumed to be constant, i.e. one value per interval. On the other hand, 
the most probable time $`mpt`$ is approximated by a 4th-order polymonial,
in terms of the distance of closest approach. So the relation is given by:

```math
mpt(d) = 0.106 - 2.031 d + 71.36 d^2 - 128.2 d^3 + 87.16 d^4
```

In Athena, the parameters are stored in an ASCII file. It is important to list 
the intervals in ascending order of distance, since the code assumes they are 
ordered that way. The file is displayed below and can be found at:
https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/share/sTGC_Digitization_timeArrival.dat

```plaintext
# Below are the parameters to be read. Line starting with '#' is not read.
# IMPORTANT: the intervals must be sorted in ascending order of low_edge.
# keyword  low_edge[mm]  k  theta
bin 0.00  0.500  1.460
bin 0.05  3.236  0.335
bin 0.15  7.270  0.219
bin 0.25  8.406  0.236
bin 0.35 10.809  0.223
bin 0.45  8.017  0.267
bin 0.55  5.859  0.366
bin 0.65  8.973  0.290
bin 0.75  9.319  0.290
bin 0.80  6.282  0.517
bin 0.85  4.680  1.044

# Most probable time is described using a 4th-order polymonial
#   mpv(d) = p0 + p1 * d + p2 * d^2 + p3 * d^3 + p4 * d^4
# keyword  p0  p1  p2  p3  p4
mpv 0.106 -2.031 71.36 -128.2 87.16
```

The helper function to read in the parameters is implemented in the 
`sTGC_Digitization/sTgcDigitMaker` class. It is initialized in the 
`sTgcDigitMaker::intialize(...)` method.
https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx

```cpp
StatusCode sTgcDigitMaker::readFileOfTimeArrival() {
  // Verify the file sTGC_Digitization_timeArrival.dat exists
  const std::string file_name = "sTGC_Digitization_timeArrival.dat";
  std::string file_path = PathResolver::find_file(file_name.c_str(), "DATAPATH");
  if(file_path.empty()) {
    ATH_MSG_FATAL("readFileOfTimeWindowOffset(): Could not find file " << file_name.c_str() );
    return StatusCode::FAILURE;
  }

  // Open the sTGC_Digitization_timeArrival.dat file
  std::ifstream ifs;
  ifs.open(file_path.c_str(), std::ios::in);
  if(ifs.bad()) {
    ATH_MSG_FATAL("sTgcDigitMaker: Failed to open time of arrival file " << file_name.c_str() );
    return StatusCode::FAILURE;
  }

  std::string line;
  GammaParameter param;

  while (std::getline(ifs, line)) {
    std::string key;
    std::istringstream iss(line);
    iss >> key;
    if (key.compare("bin") == 0) {
      iss >> param.lowEdge >> param.kParameter >> param.thetaParameter;
      m_gammaParameter.push_back(param);
    } else if (key.compare("mpv") == 0)  {
      double mpt;
      while (iss >> mpt) {m_mostProbableArrivalTime.push_back(mpt);}
    }
  }

  // Close the file
  ifs.close();
  return StatusCode::SUCCESS;
}
```
In `sTGC_Digitization`, the interval information (including parameters 
$`k`$ and $`\theta`$) is stored in an user-defined object 
`sTgcDigitMaker::GammaParameter`. It is declared in the header file:
https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitMaker.h

```cpp
  /** Parameters of a gamma probability distribution function, required for
   *  estimating wire digit's time of arrival.
   *  More detail in the dat file.
   */
  struct GammaParameter {
    double lowEdge; // low side of the interval in ns
    double kParameter;
    double thetaParameter;
  };
```

One `GammaParameter` object is created for each interval. And the 11 objects 
are stored in a `std::vector` container. A method to find and retrieve an 
interval is implemented in the `sTgcDigitMaker` class. The method assumed the
intervals are ordered, so a simple search is done to find the interval 
corresponding to the given distance of closest approach:

```cpp
sTgcDigitMaker::GammaParameter sTgcDigitMaker::getGammaParameter(double distance) const {

  double d = distance;
  if (d < 0.) {
    ATH_MSG_WARNING("getGammaParameter: expecting a positive distance, but got a negative value: " << d
                     << ". Proceed to the calculation using its absolute value.");
    d = -1.0 * d;
  }

  // Find the parameters assuming the container is sorted in ascending order of 'lowEdge'
  int index{-1};
  for (auto& par: m_gammaParameter) {
    if (distance < par.lowEdge) {
      break;
    }
    ++index;
  }
  return m_gammaParameter.at(index);
}
```

As for $`mpt`$, the parameters of the 4th-order polymonial are stored 
in a simple `std::vector` container, also in `sTgcDigitMaker`. Knowing the 
distance of closest approach, the most probable time can be evaluated 
using:

```cpp
double sTgcDigitMaker::getMostProbableArrivalTime(double distance) const {

  double d = distance;
  if (d < 0.) {
    ATH_MSG_WARNING("getMostProbableArrivalTime: expecting a positive distance, but got a negative value: " << d
                     << ". Proceed to the calculation using its absolute value.");
    d = -1.0 * d;
  }

  double mpt = m_mostProbableArrivalTime.at(0)
               + m_mostProbableArrivalTime.at(1) * d
               + m_mostProbableArrivalTime.at(2) * d * d
               + m_mostProbableArrivalTime.at(3) * d * d * d
               + m_mostProbableArrivalTime.at(4) * d * d * d * d;
  return mpt;
}
```

Notice that both `getGammaParameter(...)` and `getMostProbableArrivalTime(...)`
are defined for positive distance. If distance is negative, then the absolute value 
is used. The user should change it, if this 
behaviour is incorrect. Also both functions works with distance greater than 
0.9 mm (half wire pitch), while the time-of-arrival parametrization is only 
valid in the ranger [0, 0.9] mm. Out-of-range issues are handled before 
calling the functions.


## Computing the distance of closest approach

The first step in evaluating the time of arrival is calculating the distance 
between a particle trajectory and a wire. The calculations are done using 3D 
vectors, so independent of the coordinate frame. Therefore, the calculations 
are simpler using the coordinate frame of the wire surface. 
Below is a drawing of a particle passing through a sTGC gas gap:

<img src="impl_distanceToWire.png" width="510" height="495">

At the digitization step, the position of a hit $`\vec{p}`$ corresponds to the 
position where the particle exits the gas gap. The direction of the particle 
$`\vec{v}_{p}`$ is also known. 
The wire position $`\vec{p}_{w}`$ and wire direction $`\vec{v}_{w}`$ can be derived 
from the sTGC geometry. In the coordinate frame of the wire surface, wires 
are oriented along the $`y`$-axis, so $`\vec{v}_{w} = (0, 1, 0)`$, and the 
$`x`$-component of the wire position is given by:

```cpp
double wire_posX = detEl->positionFirstWire(id) + (wire_number - 1) * wire_pitch;
```

The $`z`$-component is zero and the $`y`$-component is set arbitrary to the same 
as the $`y`$-component of the hit position. Therefore, the smallest distance 
between a track segement and a wire is:

```math
d = \frac{|(\vec{p} - \vec{p}_{w}) \cdot (\vec{v}_{p} \times \vec{v}_{w})|}{|\vec{v}_{p} \times \vec{v}_{w}|}
```

The equation above is valid for straight track, which corresponds to 
regular cases where particles cross the gas gap between two wires. 
Since the gap thicknes is only 2.85~mm, the curvature is negligible 
for most tracks. Nevertheless, uncommon cases such as tracks with 
very low pT are not supported.

The calculations are done in `sTgcDigitMaker`:

```cpp
double sTgcDigitMaker::distanceToWire(Amg::Vector3D& position, Amg::Vector3D& direction, Identifier id, int wire_number) const
{
  // Wire number should be one or greater
  if (wire_number < 1) {
    return -9.99;
  }

  // Get the current sTGC element (a four-layer chamber)
  const MuonGM::sTgcReadoutElement* detEl = m_mdManager->getsTgcReadoutElement(id);

  // Wire number too large
  if (wire_number > detEl->numberOfWires(id)) {
    return -9.99;
  }

  // Wire pitch
  double wire_pitch = detEl->wirePitch();
  // Wire local position on the wire plane, the y-coordinate is arbitrary and z-coordinate is zero
  double wire_posX = detEl->positionFirstWire(id) + (wire_number - 1) * wire_pitch;
  Amg::Vector3D wire_position(wire_posX, position.y(), 0.);
  // The wires are parallel to Y in the wire plane's coordinate frame
  Amg::Vector3D wire_direction(0., 1., 0.);

  // Determine the sign of the distance, which is:
  //  - negative if particle crosses the wire surface on the wire_number-1 side and
  //  + positive if particle crosses the wire surface on the wire_number+1 side
  double sign = 1.0;
  if ((position.x() - wire_posX) < 0.) {
    sign = -1.0;
  }

  // Distance of closest approach is the distance between the two lines:
  //      - particle's segment
  //      - wire line

  // Find a line perpendicular to both hit direction and wire direction
  Amg::Vector3D perp_line = direction.cross(wire_direction);
  double norm_line = std::sqrt(perp_line.dot(perp_line));
  if (norm_line < 1.0e-5) {
    ATH_MSG_WARNING("Unable to compute the distance of closest approach,"
                    << " a negative value is assumed to indicate the error.");
    return -9.99;
  }
  // Compute the distance of closest approach, which is given by the projection of
  // the vector going from hit position to wire position onto the perpendicular line
  double distance = std::abs((position - wire_position).dot(perp_line) / norm_line);

  return (sign * distance);
}

```

The return distance of `distanceToWire(...)` is either positive or negative. 
The sign indicates on which side of the wire the particle passes through:

- negative if it passes through between wires `wire_number` and `wire_number-1`
- positive if it passes through between wires `wire_number` and `wire_number+1`


## Determining the nearest sTGC wire

There are many ways to determine the nearest wire to a track. The one considered 
is a simple recipe that is valid for most tracks created by charged particles 
passing through the gas gaps between two sTGC wires. With this 
assumption, the nearest wire is one of the two adjacent wires. By comparing
the distance to each wire, the nearest wire can be found. The details are listed
below:

1. Compute the hit position on the wire surface
2. Determine the wire number corresponding to the hit position
3. Compute the distance between the track and the wire
4. Compute the distance to the other adjacent wire

The implementation in `sTgcDigitMaker` is displayed below:

```cpp
  // Hit position in the wire surface's coordinate frame
  Amg::Vector3D hitOnSurface_wire = SURF_WIRE.transform().inverse() * GPOS;
  Amg::Vector2D posOnSurf_wire(hitOnSurface_wire.x(), hitOnSurface_wire.y());

  /* Determine the closest wire and the distance of closest approach
   * Since most particles pass through the the wire plane between two wires,
   * the nearest wire should be one of these two wire. Otherwise, the particle's
   * trajectory is uncommon, and such rare case is not supported yet.
   *
   * Finding that nearest wire follows the following steps:
   * - Compute the distance to the wire at the center of the current wire pitch
   * - Compute the distance to the other adjacent wire and, if it is smaller,
   *   verify the distance to the next to the adjacent wire
   */

  // hit direction in the wire surface's coordinate frame
  Amg::Vector3D loc_dire_wire = SURF_WIRE.transform().inverse().linear()*GLODIRE;

  // Wire number of the current wire pitch
  int wire_number = detEl->getDesign(layid)->wireNumber(posOnSurf_wire);

  // Compute the distance from the hit to the wire, return value of -9.99 if unsuccessful
  double dist_wire = distanceToWire(hitOnSurface_wire, loc_dire_wire, layid, wire_number);
  if (dist_wire < -9.) {
    ATH_MSG_WARNING("Failed to get the distance between the hit at ("
                    << hitOnSurface_wire.x() << ", " << hitOnSurface_wire.y() << ")"
                    << " and wire number = " << wire_number
                    << ", chamber stationName: " << stationName
                    << ", stationEta: " << stationEta
                    << ", stationPhi: " << stationPhi
                    << ", multiplet:" << multiPlet
                    << ", gas gap: " << gasGap);
  } else {
    // Determine the other adjacent wire, which is +1 if particle passes through the
    // wire plane between wires wire_number and wire_number+1 and -1 if particle
    // passes through between wires wire_number and wire_number-1
    int adjacent = 1;
    if (dist_wire < 0.) {adjacent = -1;}

    // Compute distance to the other adjacent wire
    double dist_wire_adj = distanceToWire(hitOnSurface_wire, loc_dire_wire, layid, wire_number + adjacent);
    if (std::abs(dist_wire_adj) < std::abs(dist_wire)) {
      dist_wire = dist_wire_adj;
      wire_number = wire_number + adjacent;

      // Check the next to the adjacent wire to catch uncommon track
      if ((wire_number + adjacent) > 1) {
        double tmp_dist = distanceToWire(hitOnSurface_wire, loc_dire_wire, layid, wire_number + adjacent * 2);
        if (std::abs(tmp_dist) < std::abs(dist_wire)) {
          ATH_MSG_WARNING("Wire number is more than one wire pitch away for hit position = ("
                          << hitOnSurface_wire.x() << ", " << hitOnSurface_wire.y() << ")"
                          << ", wire number = " << wire_number + adjacent * 2
                          << ", with d(-2) = " << tmp_dist
                          << ", while d(0) = " << dist_wire
                          << ", chamber stationName = " << stationName
                          << ", stationEta = " << stationEta
                          << ", stationPhi = " << stationPhi
                          << ", multiplet = " << multiPlet
                          << ", gas gap = " << gasGap);
        }
      }
    }
  }

  // Distance should be in the range [0, 0.9] mm, unless particle passes through
  // the wire plane near the edges
  double wire_pitch = detEl->wirePitch();
  // Absolute value of the distance
  double abs_dist_wire = std::abs(dist_wire);
  if ((dist_wire > -9.) && (abs_dist_wire > (wire_pitch / 2))) {
    ATH_MSG_DEBUG("Distance to the nearest wire (" << abs_dist_wire << ") is greater than expected.");
  }

  // Do not digitize hits that are too far from the nearest wire
  if (abs_dist_wire > wire_pitch) {
    return nullptr;
  }
```

The recipe discussed doesn't support all tracks.
Examples of uncommon cases that are not currently supported are:

- Charged particles with trajectory parallel (or almost parallel) to the wire surface.
- Some charged particles that doesn't have exit and/or entry position, i.e. 
  incomplete track segment that doesn't extend from the wire surface to the entry 
  or exit position.

## Evaluating the time of arrival

In `sTgcDigitMaker`, the time of arrival is evaluated using the 
`CLHEP::RandGamma` random number generator. The parameters $`k`$, $`\theta`$ 
and most probable time are determined from the distance computed in the 
previous section. The values obtained from `CLHEP::RandGamma` can be negative,
however time is always positive. Therefore, negative value is discarded and 
`CLHEP::RandGamma` is launched again to choose another value, until a positive
value is returned. To avoid runaway loop, this process is stopped and the time
is set to zero if a negative value is returned by `CLHEP::RandGamma::shoot` 
fifth times in a row.

The source code is displayed below:

```cpp
  // Get the gamma pdf parameters associated with the distance of closest approach.
  //GammaParameter gamma_par = getGammaParameter(abs_dist_wire);
  double par_kappa = (getGammaParameter(abs_dist_wire)).kParameter;
  double par_theta = (getGammaParameter(abs_dist_wire)).thetaParameter;
  double most_prob_time = getMostProbableArrivalTime(abs_dist_wire);
  // Compute the most probable value of the gamma pdf
  double gamma_mpv = (par_kappa - 1) * par_theta;
  // If the most probable value is less than zero, then set it to zero
  if (gamma_mpv < 0.) {gamma_mpv = 0.;}
  double t0_par = most_prob_time - gamma_mpv;

  // Digit time follows a gamma distribution, so a value val is
  // chosen using a gamma random generator then is shifted by t0
  // to account for drift time.
  // Note: CLHEP::RandGamma takes the parameters k and lambda,
  // where lambda = 1 / theta.
  double digit_time = t0_par + CLHEP::RandGamma::shoot(rndmEngine, par_kappa, 1/par_theta);

  // Sometimes, digit_time is negative because t0_par can be negative.
  // In such case, discard the negative value and shoot RandGamma for another value.
  // However, if that has already been done many times then set digit_time to zero
  // in order to avoid runaway loop.
  const int shoot_limit = 4;
  int shoot_counter = 0;
  while (digit_time < 0.) {
    if (shoot_counter > shoot_limit) {
      digit_time = 0.;
      break;
    }
    digit_time = t0_par + CLHEP::RandGamma::shoot(rndmEngine, par_kappa, 1/par_theta);
    ++shoot_counter;
  }

  ATH_MSG_DEBUG("sTgcDigitMaker distance = " << dist_wire
                << ", time = " << digit_time
                << ", k parameter = " << par_kappa
                << ", theta parameter = " << par_theta
                << ", most probable time = " << most_prob_time);
```

Then the digit time of the channels pad, strip and wire is set as follow:

```cp
  const float stripPropagationTime = 0.;

  float sDigitTimeWire = digit_time;
  float sDigitTimePad = sDigitTimeWire;
  float sDigitTimeStrip = sDigitTimeWire + stripPropagationTime;
```
