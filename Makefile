
arrivalTime.pdf: arrivalTime.tex $(wildcard figures/*)

	pdflatex arrivalTime.tex

.PHONY: clean

clean:
	rm -rf *.aux *.dvi *.log *.out *.nav *.snm *.toc arrivalTime.pdf
